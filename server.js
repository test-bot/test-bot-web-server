var https = require('https');
var express = require('express');
var favicon = require('static-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var Q = require('q');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var jwt = require('jsonwebtoken');
var fs = require('fs');

var queueUrl = process.env.QUEUE_URL;
var apiSettings = {
    queueUrl: queueUrl
};

var authorization = require('./app/authorization');

var api = require('./app/routes/api')(apiSettings);
var scenariosApi = require('./app/routes/scenariosApi')(apiSettings);
var suitesApi = require('./app/routes/suites')(apiSettings);
var testDataRequirementsApi = require('./app/routes/testDataRequirementsApi')(apiSettings);
var reportsApi = require('./app/routes/reports')(apiSettings);
var executedStepsApi = require('./app/routes/executedSteps')(apiSettings);

var argv = require('minimist')(process.argv.slice(2));

var app = express();

var port = process.env.PORT || 8081;

// all environments
app.set('port', port);
app.use(favicon());
app.use(logger('dev'));

// Allow CORS
app.use('/v1/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  next();
});

// Request body parsing middleware should be above methodOverride
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json({ limit: '1mb' }));

app.use(methodOverride());
app.use(cookieParser('your secret here'));
app.use(passport.initialize());

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

passport.use(
    new BearerStrategy(
        function(token, done) {
            var decoded
            try {
                decoded = jwt.verify(token, 'YAjGvRT4h58E88zX11PsQAGREm88PUC6');
                apiSettings.database.collection('users')
                    .find({accountId: decoded.sub}).limit(1)
                    .next()
                    .then(function (result) {
                        if (!result) {
                            done(null, false, 'Access denied.');
                        } else if(decoded.type !== 'web' && decoded.type !== 'api') {
                            done(null, false, 'Access denied.');
                        } else if (decoded.type === 'web' && result.lastLogout && decoded.issuedOn <= result.lastLogout) {
                            done(null, false, 'Access denied.');
                        } else if (decoded.type === 'api' && decoded.version !== result.apiTokenVersion) {
                            done(null, false, 'Access denied.');
                        } else {
                            result.permissions = decoded.permissions;
                            done(null, result, { scope: 'all' });
                        }
                    }, function (err) {
                        done(null, false, 'Access denied.');
                    });
            } catch (err) {
                done(null, false, 'Access denied.');
                return;
            }
        }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/v1/*', function (req, res, next) {
    if (!apiSettings.database) {
        return res.status(500).send('Internal error');
    }

    return next();
});

app.post('/v1/collect', api.track);

app.route('/v1/apps')
    .get(passport.authenticate('bearer', { session: false }), authorization.authorize('apps'), api.getApps)
    .post(passport.authenticate('bearer', { session: false }), authorization.authorize('apps'), api.createApp);
app.route('/v1/apps/:appId')
    .get(passport.authenticate('bearer', { session: false }), authorization.authorize('apps'), api.getApp)
    .delete(passport.authenticate('bearer', { session: false }), authorization.authorize('apps'), api.deleteApp);

app.get('/v1/apps/:appId/scenarios',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('scenarios-read'),
    api.getScenarios);

app.get('/v1/apps/:appId/scenarios/:scenarioId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('scenarios-read'),
    api.getScenario);

app.delete('/v1/apps/:appId/scenarios/:scenarioId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('scenarios-update'),
    api.deleteScenario);

app.put('/v1/apps/:appId/scenarios/:scenarioId/ignore',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('scenarios-update'),
    api.ignoreScenario);

app.get('/v1/apps/:appId/suites',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('suites-read'),
    suitesApi.get);

app.get('/v1/apps/:appId/suites/:suiteId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('suites-read'),
    suitesApi.getOne);

app.post('/v1/apps/:appId/suites',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('suites-update'),
    suitesApi.create);

app.delete('/v1/apps/:appId/suites/:suiteId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('suites-update'),
    suitesApi.delete);

app.post('/v1/apps/:appId/scenarios/generate',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('scenarios-update'),
    api.generateScenarios);

app.post('/v1/apps/:appId/scenarios/:scenarioId/assert/step',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('scenarios-assert'),
    scenariosApi.assertScenarioMutationsStep);

app.post('/v1/apps/:appId/scenarios/:scenarioId/error',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('scenarios-assert'),
    scenariosApi.saveErrorOnExecutedStep);

app.post('/v1/apps/:appId/sessions/update',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('data-update'),
    api.updateData);

app.get('/v1/apps/:appId/test-data',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-data'),
    testDataRequirementsApi.getTestData);

app.get('/v1/apps/:appId/test-data/requirements',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('data-requirements'),
    testDataRequirementsApi.getInputDataRequirements);

app.put('/v1/apps/:appId/test-data/requirements/:nodeId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('data-requirements'),
    testDataRequirementsApi.saveInputDataRequirement);

app.post('/v1/apps/:appId/test-data/requirements/analyze',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('data-requirements-update'),
    testDataRequirementsApi.analyzeInputDataRequirements);

app.get('/v1/apps/:appId/test-runs',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    reportsApi.getTestRuns);

app.get('/v1/apps/:appId/test-runs/:runId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    reportsApi.getOneTestRun);

app.post('/v1/apps/:appId/test-runs',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-update'),
    reportsApi.createTestRun);

app.put('/v1/apps/:appId/test-runs/:runId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-update'),
    reportsApi.updateTestRun);

app.get('/v1/apps/:appId/test-runs/:runId/scenario/:scenarioId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    reportsApi.getTestRunScenarioResult);

app.get('/v1/apps/:appId/test-runs/:runId/scenario/:scenarioId/steps',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    executedStepsApi.getExecutedSteps);

app.get('/v1/apps/:appId/test-runs/:runId/scenario/:scenarioId/steps/:stepId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    executedStepsApi.getExecutedStep);

app.get('/v1/apps/:appId/test-runs/baseline/:originalStepId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    executedStepsApi.getBaselineStep);

app.get('/v1/apps/:appId/test-runs/last-runs/suites',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    reportsApi.lastSuiteRuns);

app.get('/v1/apps/:appId/test-runs/history/:scenarioId',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('test-runs-read'),
    reportsApi.scenarioHistory);

app.put('/v1/apps/:appId/verification/approve',
    passport.authenticate('bearer', { session: false }),
    authorization.authorize('verification-update'),
    executedStepsApi.updateBaseline);

/*---- Mongo connection ----*/
var dbUrl = process.env.DBURL || argv.dburl;
if (dbUrl.indexOf('mongodb://') != 0) {
    // add protocol and name of the database
    dbUrl = 'mongodb://' + dbUrl + '/test-bot';
}

var MongoClient = require('mongodb').MongoClient;

// Use connect method to connect to the Server
MongoClient.connect(dbUrl, function(err, db) {
    if (err) {
        console.log("Mongo: failed to connect");
        console.log(JSON.stringify(err, null, 2));
        return;
    }

  console.log("Mongo: Connected to db: " + dbUrl);

  apiSettings.database = db; 
});

var options = {
  key: fs.readFileSync('keys/ssl.key'),
  cert: fs.readFileSync('keys/ssl.cert')
};

app.listen(port, function() {
  console.log('Node: listening on port: ' + port);
});

var secureServer = https.createServer(options, app).listen(8443, function () {
    console.log('Node SSL: listening on port: 8443');
});