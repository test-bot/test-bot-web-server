exports.authorize = function (requiredPermissions) {
    if (!Array.isArray(requiredPermissions)) {
        requiredPermissions = [requiredPermissions];
    }

    return function (req, res, next) {
        var allowed = true;
        for (var i = 0; i < requiredPermissions.length; i++) {
            var permission = requiredPermissions[i];
            if (req.user.permissions.indexOf(permission) < 0) {
                allowed = false;
                break;
            }
        }

        if (req.user.permissions.indexOf('all') >= 0) {
            allowed = true;
        }

        if (allowed) {
            next();
        } else {
            return res.status(401).send('Access denied.');
        }
    }
}