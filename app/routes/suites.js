module.exports = function (settings) {
    var validUrl = require('valid-url');
    var uuid = require('node-uuid');
    var Q = require('q');
    var AWS = require('aws-sdk');
    AWS.config.update({region: 'eu-central-1'});
    var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

    var module = {};

    module.get = function (req, res, next) {
        var collection = settings.database.collection('suites');

        collection.find({
            appId: req.params.appId
        })
        .toArray()
        .then(function (suites) {
            res.send({
                items: suites
            });
        })
        .catch(function (err) {
            res.status(500).send('Failed to get suites.');
        });
    };

    module.getOne = function (req, res, next) {
        var collection = settings.database.collection('suites');

        collection.find({
            appId: req.params.appId,
            id: req.params.suiteId
        })
        .limit(1)
        .next()
        .then(function (suite) {
            res.send(suite);
        })
        .catch(function (err) {
            res.status(500).send('Failed to get suite.');
        });
    };

    module.create = function (req, res, next) {
        var collection = settings.database.collection('suites');

        var suite = req.body;
        if (!suite.name) {
            return res.status(400).send({
                message: "The suite must have a name."
            });
        }

        if (suite.usagePercentage <= 0) {
            return res.status(400).send({
                message: "The suite must have usage percentage greater than zero."
            });
        }

        suite.id = uuid.v1();
        suite.state = 'Ready'; //TODO: should be pending, the worker should set it to ready after the tests are generated.
        suite.appId = req.params.appId;

        collection.insertOne(suite)
            .then(function (result) {
                res.send(result.ops[0]);
            })
            .then(function () {
                var params = {
                    MessageBody: JSON.stringify({
                        appId: req.params.appId,
                        accountId: req.user.accountId,
                        suiteId: suite.id
                    }),
                    QueueUrl: settings.queueUrl,
                    DelaySeconds: 0,
                    MessageAttributes: {
                        WorkType: {
                            DataType: 'String',
                            StringValue: 'GenerateScenarios'
                        }
                    }
                };

                sqs.sendMessage(params, function (err, data) {

                });
            })
            .catch(function (error) {
                res.status(500).send({
                    message: "Failed to create new a suite." 
                });
            });
    };

    module.delete = function (req, res, next) {
        var collection = settings.database.collection('suites');

        collection.updateOne({
            appId: req.params.appId,
            id: req.params.suiteId
        }, {
            $set: {state: 'Deleted'}
        })
        .then(function (result) {
            res.send('Ok');
        })
        .then(function () {
            var params = {
                MessageBody: JSON.stringify({
                    appId: req.params.appId,
                    suiteId: req.params.suiteId
                }),
                QueueUrl: settings.queueUrl,
                DelaySeconds: 0,
                MessageAttributes: {
                    WorkType: {
                        DataType: 'String',
                        StringValue: 'DeleteSuite'
                    }
                }
            };

            sqs.sendMessage(params, function (err, data) {

            });
        })
        .catch(function (error) {
            res.status(500).send({
                message: "Failed to delete suite." 
            });
        });
    };

    return module;
}