module.exports = function (settings) {
    var uuid = require('node-uuid');
    var Q = require('q');

    var module = {};

    module.getExecutedSteps = function (req, res, next) {
        var collection = settings.database.collection('executedSteps');

        collection.find({
            appId: req.params.appId,
            runId: req.params.runId,
            scenarioId: req.params.scenarioId
        })
        .project({
            mutations: 0,
            acl: 0
        })
        .sort({'timestamp': 1})
        .toArray()
        .then(function (results) {
            res.send({
                items: results
            });
        }, function (error) {
            res.status(500).send('Error when retrieving executed steps.');
        });
    };

    module.getExecutedStep = function (req, res, next) {
        var collection = settings.database.collection('executedSteps');

        collection.find({
            appId: req.params.appId,
            runId: req.params.runId,
            scenarioId: req.params.scenarioId,
            stepId: req.params.stepId
        })
        .project({
            acl: 0
        })
        .limit(1)
        .next()
        .then(function (result) {
            res.send(result);
        }, function (error) {
            res.status(500).send('Error when retrieving executed step.');
        });
    };

    module.getBaselineStep = function (req, res, next) {
        var collection = settings.database.collection('executedSteps');

        collection.find({
            appId: req.params.appId,
            originalStepId: req.params.originalStepId,
            isBaseline: true,
            isLatestBaseline: true
        })
        .project({
            acl: 0
        })
        .limit(1)
        .next()
        .then(function (result) {
            res.send(result);
        }, function (error) {
            res.status(500).send('Error when retrieving executed step.');
        });
    };

    module.updateBaseline = function (req, res, next) {
        var originalStepId = req.body.originalStepId;
        var executedStepId = req.body.executedStepId;

        // These operations will be performed on the baseline step.
        var issues = req.body.issues;

        var collection = settings.database.collection('executedSteps');
        Q.all([
            collection.find({stepId: executedStepId}).limit(1).next(),
            collection.find({originalStepId: originalStepId, isBaseline: true, isLatestBaseline: true}).limit(1).next()])
        .then(function (results) {
            var executedStep = results[0];
            var baselineStep = results[1];

            for (var i = 0; i < issues.length; i++) {
                var issue = issues[i];

                if (issue.type === 'Unexpected') {
                    var succeeded = addFromExecutedToBase(baselineStep, executedStep, issue.id);
                    if (!succeeded) {
                        return res.status(500).send({
                            message: 'The executed step does not contain specified mutation.'
                        });
                    }
                } else if (issue.type === 'Changed') {
                    var succeeded = addFromExecutedToBase(baselineStep, executedStep, issue.executed);
                    if (!succeeded) {
                        return res.status(500).send({
                            message: 'The executed step does not contain specified mutation.'
                        });
                    }

                    removeFromBase(baselineStep, issue.base);
                } else if (issue.type === 'Expected') {
                    removeFromBase(baselineStep, issue.id);
                }

                executedStep.issues = executedStep.issues.map(function (m) {
                    if (m.id === issue.id) {
                        m.verification = 'Approved';
                    }
                    return m;
                });
            }

            var baselineStepId = baselineStep.stepId;
            baselineStep._id = null;
            baselineStep.stepId = uuid.v1();
            baselineStep.baselineVersion += 1;

            return collection.insertOne(baselineStep)
                .then(function () {
                    return collection.findOneAndUpdate({
                        stepId: baselineStepId
                    }, {
                        '$set': {
                            isLatestBaseline: false
                        }
                    });
                })
                .then(function () {
                    return collection.findOneAndUpdate({
                        stepId: executedStepId
                    }, {
                        '$set': {
                            issues: executedStep.issues
                        }
                    })
                });
        })
        .then(function (results) {
            return res.send('Ok');
        }, function (err) {
            return res.status(500).send('Error when approving changes.');
        });
    };

    function addFromExecutedToBase(baselineStep, executedStep, toAddId) {
        var toAdd = executedStep.mutations.filter(function (m) {
            return m.mutationId === toAddId;
        });

        if (toAdd.length === 0) {
            return false;
        } else {
            toAdd = toAdd[0];
            toAdd.timestamp = null;
            baselineStep.mutations.push(toAdd);
            return true;
        }
    }

    function removeFromBase(baselineStep, toRemoveId) {
        baselineStep.mutations = baselineStep.mutations.filter(function (m) {
            return m.mutationId !== toRemoveId;
        });
    }

    return module;
}