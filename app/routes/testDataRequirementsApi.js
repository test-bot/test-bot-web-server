module.exports = function (settings) {
    var AWS = require('aws-sdk');
    var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

    var module = {};

    module.getInputDataRequirements = function (req, res, next) {
        var collection = settings.database.collection('nodesInfo');

        var appId = req.params.appId;

        collection.find({
            appId: appId,
            checkedForDataRequirement: true,
            'acl.canRead': req.user.accountId
        })
        .project({
            targetSelector: 1,
            targetOuterHTML: 1,
            nodeId: 1,
            testData: 1,
            requiresData: 1,
            inputType: 1,
            type: 1
        })
        .toArray()
        .then(function (nodes) {
            res.send({
                items: nodes
            });

            return nodes;
        });
    };

    module.getTestData = function(req, res, next) {
        var collection = settings.database.collection('nodesInfo');

        var appId = req.params.appId;

        collection.find({
            appId: appId,
            requiresData: true,
            'acl.canRead': req.user.accountId
        })
        .project({
            nodeId: 1,
            testData: 1,
            requiresData: 1
        })
        .toArray()
        .then(function (nodes) {
            res.send({
                items: nodes
            });

            return nodes;
        })
    }

    module.analyzeInputDataRequirements = function (req, res, next) {
        var collection = settings.database.collection('applications');
        // Check if this account has permissions for the specified app.
        collection.findOne({
            accountId: req.user.accountId,
            id: req.params.appId
        })
        .then(function (result) {
            return !!result;
        })
        .then(function (verified) {
            if (verified) {
                var params = {
                    MessageBody: JSON.stringify({
                        appId: req.params.appId,
                        accountId: req.user.accountId
                    }),
                    QueueUrl: settings.queueUrl,
                    DelaySeconds: 0,
                    MessageAttributes: {
                        WorkType: {
                            DataType: 'String',
                            StringValue: 'RecognizeInputDataRequirements'
                        }
                    }
                };

                sqs.sendMessage(params, function (err, data) {
                    if (err) {
                        res.status(500).send('Failed to schedule analyzing of input data requirements. Please try again.');
                        return;
                    }

                    res.send({
                        message: 'Input data requirements will be updated soon.'
                    });
                });
            } else {
                return res.status(401).send('Access denied.');
            }
        });
    };

    module.saveInputDataRequirement = function (req, res, next) {
        var appId = req.params.appId;
        var nodeId = req.params.nodeId;
        var testData = req.body.testData;
        var requiresData = req.body.requiresData;

        var collection = settings.database.collection('nodesInfo');
        collection.updateOne({
            nodeId: nodeId,
            'acl.canUpdate': req.user.accountId
        }, {
            $set: {
                testData: testData,
                requiresData: requiresData
            }
        })
        .then(function (result) {
            return res.status(200).send('OK');
        })
        .catch(function (error) {
            return res.status(500).send('Failed to update test data.');
        });
    };

    return module;
}