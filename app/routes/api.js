module.exports = function (settings) {
    var validUrl = require('valid-url');
    var uuid = require('node-uuid');
    var collect = require('../processing/collect');
    var AWS = require('aws-sdk');
    var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

    var module = {};

    module.track = function (req, res, next) {
        var collection = settings.database.collection('sentences');

        var sentences = collect.getSentencesFromRequest(req.body);

        collection.insert(sentences, function (err, result) {
            if (err) {
                return res.status(500).send(err);
            }

            return res.send(true);
        });
    };

    module.createApp = function (req, res, next) {
        var collection = settings.database.collection('applications');

        var app = req.body;
        if (!app.name) {
            return res.status(400).send({
                message: "The app must have name."
            });
        }

        if (!app.url) {
            return res.status(400).send({
                message: "The app must have url."
            });
        } else if(!validUrl.isUri(app.url)) {
            return res.status(400).send({
                message: "The app must have valid url."
            });
        }

        app.id = uuid.v1();
        app.accountId = req.user.accountId;
        app.state = 'Pending';

        collection.insertOne(app)
            .then(function (result) {
                res.send(result.ops[0]);
            })
            .then(function () {
                var params = {
                    MessageBody: JSON.stringify({
                        appId: app.id
                    }),
                    QueueUrl: settings.queueUrl,
                    DelaySeconds: 0,
                    MessageAttributes: {
                        WorkType: {
                            DataType: 'String',
                            StringValue: 'CreateApplication'
                        }
                    }
                };

                sqs.sendMessage(params, function (err, data) {

                });
            })
            .catch(function (error) {
                res.status(500).send({
                    message: "Failed to create new app." 
                });
            });
    };

    module.deleteApp = function (req, res, next) {
        var collection = settings.database.collection('applications');

        collection.updateOne({
            id: req.params.appId,
        }, {
            $set: {state: 'Deleted'}
        })
        .then(function (result) {
            var params = {
                MessageBody: JSON.stringify({
                    appId: req.params.appId
                }),
                QueueUrl: settings.queueUrl,
                DelaySeconds: 0,
                MessageAttributes: {
                    WorkType: {
                        DataType: 'String',
                        StringValue: 'DeleteApplication'
                    }
                }
            };

            sqs.sendMessage(params, function (err, data) {
                res.send('Ok');
            });
        });
    };

    module.getApps = function (req, res, next) {
        var collection = settings.database.collection('applications');

        collection.find({
            accountId: req.user.accountId
        })
        .toArray()
        .then(function (apps) {
            res.send({
                items: apps
            });
        })
        .catch(function (err) {
            res.status(500).send('Failed to get apps.');
        });
    };

    module.getApp = function (req, res, next) {
        var collection = settings.database.collection('applications');

        collection.find({
            accountId: req.user.accountId,
            id: req.params.appId
        })
        .limit(1)
        .next(function (err, app) {
            if (err) {
                res.status(500).send('Failed to get app.');
                return;
            }
            res.send(app);
        });
    };

    module.getScenarios = function (req, res, next) {
        var collection = settings.database.collection('scenarios');

        collection.find({
            appId: req.params.appId,
            'acl.canRead': req.user.accountId
        })
        .toArray(function (err, scenarios) {
            if (err) {
                res.status(500).send('Failed to get scenarios.');
                return;
            }

            res.send({
                items: scenarios
            });
        });
    };

    module.getScenario = function (req, res, next) {
        var collection = settings.database.collection('scenarios');

        collection.find({
            appId: req.params.appId,
            scenarioId: req.params.scenarioId,
            'acl.canRead': req.user.accountId
        })
        .limit(1)
        .next()
        .then(function (scenario) {
            res.send(scenario);
        }, function () {
            res.status(500).send('Failed to get scenario.');
        });
    }

    module.deleteScenario = function (req, res, next) {
        var collection = settings.database.collection('scenarios');

        collection.updateOne({
            appId: req.params.appId,
            scenarioId: req.params.scenarioId
        }, {
            $set: {state: 'Deleted'}
        })
        .then(function (result) {
            res.send('Ok');
        })
        .then(function () {
            var params = {
                MessageBody: JSON.stringify({
                    appId: req.params.appId,
                    scenarioId: req.params.scenarioId
                }),
                QueueUrl: settings.queueUrl,
                DelaySeconds: 0,
                MessageAttributes: {
                    WorkType: {
                        DataType: 'String',
                        StringValue: 'DeleteScenario'
                    }
                }
            };

            sqs.sendMessage(params, function (err, data) {

            });
        })
        .catch(function (error) {
            res.status(500).send({
                message: "Failed to delete scenario." 
            });
        });
    };

    module.ignoreScenario = function (req, res, next) {
        var collection = settings.database.collection('scenarios');

        collection.updateOne({
            appId: req.params.appId,
            scenarioId: req.params.scenarioId
        }, {
            $set: {ignored: !!req.body.ignored}
        })
        .then(function (result) {
            res.send('Ok');
        })
        .catch(function (error) {
            res.status(500).send({
                message: "Failed to ignore/unignore scenario." 
            });
        });
    };

    module.generateScenarios = function (req, res, next) {
        var collection = settings.database.collection('applications');
        // Check if this account has permissions for the specified app.
        collection.findOne({
            accountId: req.user.accountId,
            id: req.params.appId
        })
        .then(function (result) {
            return !!result;
        })
        .then(function (verified) {
            if (verified) {
                var params = {
                    MessageBody: JSON.stringify({
                        appId: req.params.appId,
                        accountId: req.user.accountId,
                        suiteId: req.body.suiteId
                    }),
                    QueueUrl: settings.queueUrl,
                    DelaySeconds: 0,
                    MessageAttributes: {
                        WorkType: {
                            DataType: 'String',
                            StringValue: 'GenerateScenarios'
                        }
                    }
                };

                sqs.sendMessage(params, function (err, data) {
                    if (err) {
                        res.status(500).send('Failed to schedule scenarios regeneration. Please try again.');
                        return;
                    }

                    res.send({
                        message: 'Scenarios will be generated soon.'
                    });
                });
            } else {
                return res.status(401).send('Access denied.');
            }
        })
        .catch(function (err) {
            res.status(401).send('Access denied.');
        });
    };

    module.updateData = function (req, res, end) {
        var collection = settings.database.collection('applications');

        var updatePeriod = 10;

        collection.find({
            id: req.params.appId,
            accountId: req.user.accountId
        })
        .limit(1)
        .next(function (err, app) {
            if (err) {
                res.status(500).send('Failed to get the app.');
                return;
            }

            if(!app) {
                res.status(401).send('Access denied.');
                return;
            }

            var shouldUpdate;
            var timeDiffSec
            if(app.lastTimeDataUpdated) {
                var timeDiff = new Date().getTime() - new Date(app.lastTimeDataUpdated).getTime();
                timeDiffSec = timeDiff / 1000;
                shouldUpdate = timeDiffSec > updatePeriod;
            } else {
                shouldUpdate = true;
            }

            if(shouldUpdate) {
                var params = {
                    MessageBody: JSON.stringify({
                        appId: req.params.appId
                    }),
                    QueueUrl: settings.queueUrl,
                    DelaySeconds: 0,
                    MessageAttributes: {
                        WorkType: {
                            DataType: 'String',
                            StringValue: 'PromoteForSessionCalculation'
                        }
                    }
                };

                sqs.sendMessage(params, function (err, data) {
                    if (err) {
                        res.status(500).send('Failed to schedule a data update. Please try again.');
                        return;
                    }

                    var lastTimeUpdated = new Date();
                    collection.updateOne({
                            id: req.params.appId
                        },
                        {
                            $set: {
                                lastTimeDataUpdated: lastTimeUpdated
                            }
                        })
                    .then(function () {
                        res.send({
                            state: 0,
                            message: 'The data will be updated soon.',
                            nextAvailableUpdateAfterSec: updatePeriod,
                            lastTimeDataUpdated: lastTimeUpdated
                        });
                    });
                });
            } else {
                var secRemaining = updatePeriod - timeDiffSec;
                res.send({
                    state: 1,
                    message: 'Update will be possible after ' + secRemaining + ' seconds.',
                    nextAvailableUpdateAfterSec: secRemaining,
                    lastTimeDataUpdated: app.lastTimeDataUpdated
                });
            }
        });
    }

    return module;
};