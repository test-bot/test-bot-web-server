module.exports = function (settings) {
    var module = {};

    module.createTestRun = function (req, res, next) {
        var collection = settings.database.collection('testRuns');

        var appId = req.body.appId;
        var runId = req.body.runId;
        var machineName = req.body.machineName;
        var started = req.body.started;

        collection.insertOne({
            appId: appId,
            runId: runId,
            machineName: machineName,
            started: new Date(started)
        })
        .then(function (result) {
            res.send('Ok');
        })
        .catch(function (error) {
            res.status(500).send('Failed to register new test run.');
        });
    };

    module.updateTestRun = function (req, res, next) {
        var collection = settings.database.collection('testRuns');

        var runId = req.params.runId;

        collection.updateOne({
            runId: runId
        }, {
                $set: {
                    ended: new Date(req.body.ended),
                    failed: req.body.failed,
                    passed: req.body.passed,
                    scenarios: convertStringDatesToDates(req.body.scenarios)
                }
        })
        .then(function (result) {
            res.send('Ok');
        })
        .catch(function (error) {
            res.status(500).send('Failed to update test run statistics.');
        });
    };

    module.getTestRuns = function (req, res, next) {
        var collection = settings.database.collection('testRuns');

        collection.find({appId: req.params.appId})
            .project({
                scenarios: 0
            })
            .sort({'started': -1})
            .toArray()
            .then(function (results) {
                res.send({
                    items: results
                });
            }, function (error) {
                res.status(500).send('Error when retrieving test results.');
            });
    };

    module.getOneTestRun = function (req, res, next) {
        var collection = settings.database.collection('testRuns');

        collection.find({
            appId: req.params.appId,
            runId: req.params.runId
        })
        .limit(1)
        .next()
        .then(function (result) {
            res.send(result);
        }, function (error) {
            res.status(500).send('Error when retrieving a test result.');
        });
    };

    module.getTestRunScenarioResult = function (req, res, next) {
        var collection = settings.database.collection('testRuns');

        collection.find({
            appId: req.params.appId,
            runId: req.params.runId,
            'scenarios.scenarioId': req.params.scenarioId
        })
        .limit(1)
        .next()
        .then(function (result) {
            var scenario = result.scenarios.filter(function (test) {
                return test.scenarioId === req.params.scenarioId;
            })[0];

            res.send(scenario);
        }, function (error) {
            res.status(500).send('Error when retrieving a test result.');
        });
    };

    module.lastSuiteRuns = function (req, res, next) { //TODO: make it return info for each suite once many suites are implemented.
        var collection = settings.database.collection('testRuns');

        collection.find({ 
            appId: req.params.appId
        })
        .project({
            scenarios: 0
        })
        .sort({'started': -1})
        .limit(1)
        .next()
        .then(function (result) {
            res.send({
                items: result ? [result] : []
            });
        }, function (error) {
            res.status(500).send('Error when retrieving the last test results.');
        });
    };

    module.scenarioHistory = function (req, res, next) {
        var collection = settings.database.collection('testRuns');

        collection.find({
            'scenarios.scenarioId': req.params.scenarioId
        })
        .project({
            'scenarios.$': 1,
            runId: 1,
            machineName: 1,
            started: 1,
            ended: 1,
            passed: 1,
            failed: 1
        })
        .sort({'started': -1})
        .limit(req.query.take ? parseInt(req.query.take) : 100)
        .toArray()
        .then(function (results) {
            res.send({
                items: results
            });
        }, function (error) {
            res.status(500).send('Error when retrieving a scenario history.');
        });
    };

    function convertStringDatesToDates(scenarios) {
        return scenarios.map(function (scenario) {
            scenario.started = new Date(scenario.started);
            scenario.ended = new Date(scenario.ended);
            return scenario;
        });
    }

    return module;
};