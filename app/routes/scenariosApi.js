module.exports = function (settings) {
    var validUrl = require('valid-url');
    var uuid = require('node-uuid');
    var Q = require('q');
    var AWS = require('aws-sdk');
    AWS.config.update({region: 'eu-central-1'});
    var sqs = new AWS.SQS({apiVersion: '2012-11-05'});
    var s3 = new AWS.S3({apiVersion: '2006-03-01'});
    var lambda = new AWS.Lambda({apiVersion: '2015-03-31'});

    var mutationsBucketName = 'test-bot-mutations-de';
    var bucketRegion = "eu-central-1";

    var module = {};

    module.assertScenarioMutationsStep = function (req, res, end) {
        var appId = req.params.appId;
        var scenarioId = req.params.scenarioId;

        var eventNodeId = req.body.nodeId;
        var mutations = req.body.mutations;
        var runId = req.body.runId;
        var originalStepId = req.body.stepId;

        var collection = settings.database.collection('executedSteps');

        var executedStepId = uuid.v1();

        var executedStep = {
            appId: appId,
            scenarioId: scenarioId,
            nodeId: eventNodeId,
            runId: runId,
            targetSelector: req.body.targetSelector,
            type: req.body.type
        };

        collection.find({originalStepId: originalStepId, isBaseline: true, isLatestBaseline: true}).limit(1).next()
            .then(function (node) {
                if (node) {
                    if (node.acl.canRead.indexOf(req.user.accountId) < 0 ||
                        node.acl.canUpdate.indexOf(req.user.accountId) < 0) {
                        res.satus(401).send('Access denied.');
                        throw 'Access denied.';
                    } else {
                        return node;
                    }
                } else return false;
            })
            .then(function(baseline) {
                if (baseline) {
                    return saveExecutedStep(executedStep, mutations, executedStepId, originalStepId, req.user.accountId)
                        .then(function (executedStep) {
                            return assertMutations(baseline.mutations, executedStep.mutations);
                        });
                } else {
                    return saveExecutedStep(executedStep, mutations, executedStepId, originalStepId, req.user.accountId, true)
                        .then(function () {
                            return {
                                assertStatus: 'passed'
                            };
                        });
                }
            })
            .then(function (data) {
                saveExecutedStepStatus(executedStep, data);
                return res.send(data);
            })
            .catch(function (error) {
                return res.status(500).send(error);
            });
    };

    function saveExecutedStep(executedStep, mutations, stepId, originalStepId, accountId, isBaseline) {
        for (var i = 0; i < mutations.length; i++) {
            var mutationId = uuid.v1();
            mutations[i].mutationId = mutationId;
        }

        executedStep.mutations = mutations;
        executedStep.isBaseline = isBaseline;
        executedStep.isLatestBaseline = isBaseline;
        executedStep.stepId = stepId;
        executedStep.originalStepId = originalStepId;
        executedStep.timestamp = Date.now();

        if (isBaseline) {
            executedStep.baselineVersion = 1;
        }

        executedStep.acl = {
            canRead: [accountId],
            canUpdate: [accountId]
        };

        var collection = settings.database.collection('executedSteps');
        return collection.insertOne(executedStep)
            .then(function (result) {
                return result.ops[0];
            });
    }

    function saveExecutedStepStatus(executedStep, assertData) {
         var collection = settings.database.collection('executedSteps');

         return collection.findOneAndUpdate({
                 'stepId': executedStep.stepId
             }, {
                 '$set': {
                     status: assertData.assertStatus,
                     errors: assertData.errors,
                     issues: assertData.issues
                 }
             });
    }

    function assertMutations(nodeMutations, executedMutations) {
        return Q.all(nodeMutations.map(function (nodeMutation, index) {
            var executedMutation = executedMutations.filter(function(executedMutation) {
                if (!executedMutation.targetSelector) {
                    executedMutation.targetSelector = null;
                }
                return nodeMutation.targetSelector === executedMutation.targetSelector &&
                    nodeMutation.mutatedElementSelector === executedMutation.mutatedElementSelector &&
                    nodeMutation.type === executedMutation.type;
            });
            if (executedMutation.length === 0) {
                return Q.fcall(function (){
                    return {
                        assertStatus: 'failed',
                        error: 'Expected html mutation did not occurred: ' +
                            nodeMutation.mutatedElementSelector +
                            ' to be ' +
                            nodeMutation.type,
                        issue: {
                            id: nodeMutation.mutationId,
                            type: 'Expected'
                        }
                    };
                });
            } else {
                return Q.fcall(function (){
                    if (nodeMutation.type === 'Added') {
                        return assertAddedMutation(nodeMutation, executedMutation[0], mutationsBucketName, bucketRegion);
                    }

                    return {
                        assertStatus: 'passed'
                    };
                });
            }
        }))
        .then(function (mutationsResults) {
            var endResult = {
                assertStatus: 'passed',
                errors: [],
                issues: []
            };

            for (var i = 0; i < mutationsResults.length; i++) {
                var result = mutationsResults[i];
                if (result.assertStatus === 'failed') {
                    endResult.assertStatus = 'failed';
                    endResult.errors.push(result.error);
                    if (result.issue) {
                        endResult.issues.push(result.issue);
                    }
                }
            }

            return endResult;
        });
    }

    function assertAddedMutation(nodeMutation, executedMutation, mutationsBucketName, bucketRegion) {
        return invokeLambda(nodeMutation, executedMutation, mutationsBucketName, bucketRegion);
    }

    function invokeLambda(nodeMutation, executedMutation, bucketName, region) {
        var data = {
            oldSource: nodeMutation.outerHTML,
            newSource: executedMutation.outerHTML,
            bucketName: bucketName,
            region: region
        };
        var params = {
            FunctionName: 'test-bot-diff', /* required */
            InvocationType: 'RequestResponse',
            LogType: 'Tail',
            Payload: JSON.stringify(data)
        };
        var deferred =  Q.defer();
        lambda.invoke(params, function(err, data) {
            if (err) {
                deferred.reject(err);
            } else if (data.FunctionError) {
                deferred.reject(data.Payload);
            }
            else {
                var payload = JSON.parse(data.Payload);
                var collection = settings.database.collection('executedSteps');

                collection.findOneAndUpdate({
                        'mutations.mutationId': executedMutation.mutationId
                    }, {
                        '$set': {
                            'mutations.$.report': payload.report,
                            'mutations.$.changes': payload.changes
                        }
                    });

                if (payload.changes && payload.changes.length > 0) {
                    deferred.resolve({
                        assertStatus: 'failed',
                        error: 'The ' + executedMutation.type + ' ' + executedMutation.mutatedElementSelector + ' has unexpected changes.',
                        changes: payload.changes,
                        issue: {
                            base: nodeMutation.mutationId,
                            executed: executedMutation.mutationId,
                            type: 'Changed'
                        }
                    });
                } else {
                    deferred.resolve({
                        assertStatus: 'passed'
                    });
                }
            }
        });

        return deferred.promise;
    }

    module.saveErrorOnExecutedStep = function (req, res, next) {
        var appId = req.params.appId;
        var scenarioId = req.params.scenarioId;

        var eventNodeId = req.body.nodeId;
        var error = req.body.error;
        var runId = req.body.runId;
        var baselineStepId = req.body.stepId;

        var collection = settings.database.collection('executedSteps');

        var accountId = req.user.accountId;

        var executedStepId = uuid.v1();

        var executedStep = {
            stepId: executedStepId,
            appId: appId,
            scenarioId: scenarioId,
            nodeId: eventNodeId,
            runId: runId,
            targetSelector: req.body.targetSelector,
            type: req.body.type,
            status: 'failed',
            errors: [error],
            timestamp: Date.now(),
            acl: {
                canRead: [accountId],
                canUpdate: [accountId]
            }
        };

        var collection = settings.database.collection('executedSteps');
        return collection.insertOne(executedStep)
            .then(function (result) {
                return res.send('Ok');
            });
    };

    return module;
}