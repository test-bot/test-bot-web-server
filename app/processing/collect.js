module.exports.getSentencesFromRequest = function (requestBody) {
    var sentences = [];

    // Make sure only the needed properties will go to the database.
    var events = requestBody.events.map(function (event) {
        var entry = {
            activityType: 'Event',
            targetSelector: event.targetSelector,
            type: event.type,
            timestamp: event.timestamp,
            processed: false,
            userId: event.userId,
            appId: event.appId,
            pageUrl: event.pageUrl
        };

        // TODO: sanitize fields for unwanted html or js
        if (event.fields) {
            for (var i = 0; i < event.fields.length; i++) {
                var field = event.fields[i];

                if (typeof field === 'string' || field instanceof String) {
                    entry[field] = event[field];
                } else {
                    for (var key in field) {
                        if (field.hasOwnProperty(key)) {
                            entry[key] = event[key];
                        }
                    }
                }
            }
        }

        return entry;
    });

    var mutations = requestBody.mutations.map(function (mutation) {
        return {
            activityType: 'Mutation',
            targetSelector: mutation.targetSelector,
            mutatedElementSelector: mutation.mutatedElementSelector,
            type: mutation.type,
            timestamp: mutation.timestamp,
            processed: false,
            userId: mutation.userId,
            appId: mutation.appId,
            pageUrl: mutation.pageUrl
        };
    });

    Array.prototype.push.apply(sentences, events);
    Array.prototype.push.apply(sentences, mutations);

    return sentences;
};